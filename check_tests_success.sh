failures=$(grep -Po "(?<=failures=\")[0-9]+" report.xml)
errors=$(grep -Po "(?<=errors=\")[0-9]+" report.xml)
[ $failures -eq 0 ] && [ $errors -eq 0 ]  || exit 1
