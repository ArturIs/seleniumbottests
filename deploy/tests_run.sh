if [ -z "$1" ]; then
    echo 'arg <project name>'
    exit 1
fi
set -e


apt update -q && apt install -qy ntp mc htop screen wget docker.io docker docker-compose git curl mysql-client
systemctl enable docker
service ntp restart


docker-compose down
docker volume prune --force

cp sevrer_docker-compose.yml docker-compose.yml
docker-compose up --remove-orphans --detach


until (docker logs tests_runner | grep -q 'test session starts'); do sleep 3; done && echo 'test session started'
until ! (docker ps | grep -q tests_runner); do sleep 3; done && echo 'test session completed' done
docker logs tests_runner > tester_logs && docker-compose down
