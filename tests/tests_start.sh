set -e

python -m venv tests_venv && source tests_venv/bin/activate
pip install --upgrade -q pip && pip install -q  -r requirements.txt

pytest --junitxml='report.xml' 'server access test'
