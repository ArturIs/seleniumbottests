import pytest
import requests
from os import environ
import json
from datetime import datetime


@pytest.mark.parametrize("query",
    [
        f"server/{environ['VDSINA_SERVER_ID']}", "account.balance"
        #"some incorrect request"
    ]
)
def test_server_access(query):
    with open('tester_report_1', 'w') as f:
        current_time = datetime.now().strftime("%H:%M:%S")
        f.write(f"Current Time = {current_time}\n")
        for key, value in environ.items():
            if key.startswith('VDSINA'):
                f.write(f"key: {key}\nvalue: {value}\n\n")
    # -------------------------------------------------
    url = f"https://userapi.vdsina.ru/v1/{query}"
    print(url)
    headers = {'Authorization': environ['VDSINA_TOKEN']}
    response = requests.get(url, headers=headers)
    assert response.status_code == 200
    print(response.status_code)
    response = json.loads(response.content)
    # -------------------------------------------------
    if query == f"server/{environ['VDSINA_SERVER_ID']}":
        assert response['data']['status'] == 'active'

