from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common import exceptions
from selenium.webdriver.support import expected_conditions as EC
import time
from datetime import datetime
from os import environ


def test_account_login():
    capabilities = webdriver.FirefoxOptions().to_capabilities()
    attempts, driver = 0, None
    while attempts < 10:
        try:
            driver = webdriver.Remote(command_executor='http://firefox_selenium:4444/wd/hub',
                                      desired_capabilities=capabilities)
            break
        except:
            driver = None
            attempts += 1
            time.sleep(2)
    if not driver:
        print("SELENIUM DOCKER CONTAINER UNAVAILABLE NOW")
        exit(-1)

    wait = WebDriverWait(driver, timeout=10, ignored_exceptions=[exceptions.NoSuchElementException]).until
    driver.get('https://cp.vdsina.ru/login')
    # ----------------------------------------------------------------
    email_box = wait(EC.presence_of_element_located((By.NAME, 'email')))
    passw_box = wait(EC.presence_of_element_located((By.NAME, 'password')))
    email_box.send_keys(environ["VDSINA_MAIL"])
    passw_box.send_keys(environ["VDSINA_PASSW"])
    login_btn = wait(EC.presence_of_element_located((By.CLASS_NAME, 'btn.btn-submit.btn-primary.btn-block')))
    login_btn.click()
    time.sleep(1)
    # ----------------------------------------------------------------
    logout_btn = wait(EC.presence_of_element_located((By.XPATH, "/html/body/div[1]/div[1]/nav/div/div[2]/a[3]")))
    wait(EC.element_to_be_clickable, logout_btn)
    logout_btn.click()
